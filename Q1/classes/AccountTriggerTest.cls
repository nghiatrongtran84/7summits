@isTest
public class AccountTriggerTest {
    static testMethod void testAfterInsertSuccessCase(){
        // create a new Accounts for example
        Account acctObj = new Account(Name='TestAccount');
              
        Test.startTest();
        Database.SaveResult result = Database.insert(acctObj, false);
        Test.stopTest();
        // so check that we got back an error.
        System.assert(result.isSuccess());
       
        // check create 2 Opportunities: Default Residential Opp + Default Commercial Opp 
        Set<String> OppNames = new Set<String>();
        OppNames.add('Default Residential Opp');
        OppNames.add('Default Commercial Opp');
        List<Opportunity> listOppt = [select id,Name from Opportunity where AccountId =: acctObj.Id and Name IN :OppNames];
        
        System.assert(listOppt.size() == 2);
    }
    
    
}