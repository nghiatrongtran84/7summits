/*************************************************************************************************************************
 * Class Name          : AccountTriggerHandler
 * Description         : Class consolicates all Account trigger functionalities
 * Author              : Nghia Tran
 * Created On          : 04-May-2020
 * Mofification Log    :
 * ***********************************************************************************************************************/
public with sharing class AccountTriggerHandler {
	/// handle recursive trigger
    public static Boolean isFirstTime = true;
    /// handle trigger size
    private integer BatchSize = 0;
    
    public AccountTriggerHandler(integer size)
    {
        BatchSize = size;
    }
    
    public void OnAfterInsert(List<Account> newAccount)
    {
        List<Opportunity> optyList=new List<Opportunity>();

        for(Account acctObj : newAccount)
        {
            // Create Default Residential Opp
            Opportunity oppRes = new Opportunity();
            oppRes.AccountId   = acctObj.id;
            oppRes.Name        = 'Default Residential Opp';
            oppRes.StageName   = 'Prospecting';
            oppRes.CloseDate   = Date.toDay().addDays(30);
            optyList.add(oppRes);
            
            // Create Default Commercial Opp
            Opportunity oppCom = new Opportunity();
            oppCom.AccountId   = acctObj.id;
            oppCom.Name        = 'Default Commercial Opp';
            oppCom.StageName   = 'Prospecting';
            oppCom.CloseDate   = Date.toDay().addDays(30);
            optyList.add(oppCom);
        }
        Database.SaveResult[] results = Database.insert(optyList, false);

        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully insert Opportunity.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' + err.getStatusCode() +' - ' + err.getMessage());
                }
            }       
        }
    }
    
    
}