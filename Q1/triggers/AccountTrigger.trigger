trigger AccountTrigger on Account (after insert) {
     AccountTriggerHandler handler = new AccountTriggerHandler(Trigger.size);
     if( Trigger.isInsert )
     {
        if(Trigger.isAfter){
            
            if(AccountTriggerHandler.isFirstTime)
            {
                AccountTriggerHandler.isFirstTime = false;
                handler.OnAfterInsert(trigger.New);
            }
            
        }
     }
     
}