<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Commercial_Opp_Missing</fullName>
        <description>Commercial Opp Missing</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Commercial_Opp_Missing</template>
    </alerts>
    <alerts>
        <fullName>Residential_Opp_Missing</fullName>
        <description>Residential Opp Missing</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Residential_Opp_Missing</template>
    </alerts>
    <rules>
        <fullName>Send email when Commercial Opp is missing</fullName>
        <actions>
            <name>Commercial_Opp_Missing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Roll_Up_Commercial_Opp__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Send email when Commercial Opp is missing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email when Residential Opp is missing</fullName>
        <actions>
            <name>Residential_Opp_Missing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Roll_Up_Residential_Opp__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Send email when Residential Opp is missing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
